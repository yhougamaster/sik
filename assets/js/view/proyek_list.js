    
    window.onload = hideErrorMessages();

    function hideErrorMessages(){
        console.log("MASUK?");
        $("#error_idproyek").hide();
        $("#error_namaproyek").hide();
        $("#error_customer").hide();
        $("#error_reportUser").hide();
        $("#error_flagStatus").hide();
        $("#edit-error_email").hide();
        $("#edit-error_email2").hide();
        $("#edit-error_email3").hide();
        $("#edit-error_username").hide();
        $("#edit-error_username2").hide();
        $("#edit-error_fullname").hide();
        $("#edit-error_fullname2").hide();
        $("#edit-error_roleid").hide();
        hide_loading();
    }

    $(document).ready( function () {

        //$('#dataTables-user-log').DataTable();
        $('#dataTables-proyek-list').DataTable({
            "bFilter": true,
            "paging":   false,
            //"iDisplayLength": 20,
            "order": [[ 0, "asc" ]]
            //"bDestroy": true,
        });
     } );

    function edit_user_popup(username,email,id,fullname,roleid){
        $( "#edit-username" ).val(username);
        $( "#edit-fullname" ).val(fullname);
        $( "#edit-email" ).val(email);
        $( "#edit-user-id" ).val(id);
        $( "#edit-roleid" ).val(roleid);
        if(roleid=='1')
            roleOption = "<option value='1' selected>Admin</option><option value='2'>Pegawai</option><option value='3'>Approval</option>";
        if(roleid=='2')
            roleOption = "<option value='1'>Admin</option><option value='2' selected>Pegawai</option><option value='3'>Approval</option>";
        if(roleid=='3')
            roleOption = "<option value='1'>Admin</option><option value='2' >Pegawai</option><option value='3' selected>Approval</option>";

        $( "#edit-roleid" ).html(roleOption);
        $('#editUserSubmit').attr("onclick","update_user_details("+username+")");
    }

    function deactivate_confirmation(username,id){
        $( "#user-username" ).html(username);
        $('#deactivateYesButton').attr("onclick","deactivate_submit('"+username+"',"+id+")");
    }

    function reset_confirmation(email,id){
        $( "#reset-user-email" ).html(email);
        $('#resetYesButton').attr("onclick","reset_submit('"+email+"',"+id+")");
    }

    function deactivate_submit(username,id){
        show_loading();
            $.ajax({
            url: $("#base-url").val()+"admin/deactivate_user/"+username+"/"+id,
            cache: false,
            success: function (hasil) {
                var result = JSON.parse(hasil);
                if(result.status=="success"){
                    location.reload();
                }
                else{
                    alert("Oops there is something wrong!");
                }
            },
            error: ajax_error_handling
        });
    }

    function reset_submit(email,id){
        show_loading();
            $.ajax({
            url: $("#base-url").val()+"admin/reset_user_password/"+email+"/"+id,
            cache: false,
            success: function (result) {
                var result = $.parseJSON(result);
                if(result.status=='success'){
                    location.reload();
                }
                else{
                    alert("Oops there is something wrong!");
                }
            },
            error: ajax_error_handling
        });
    }

    function update_user_details(username){
        console.log(username);
        hideErrorMessages();
        show_loading();
        var i=0;
        var name = $('#edit-username').val();
        var email = $('#edit-email').val();
        var role = $('#edit-roleid').val();
        var username = $('#edit-fullname').val();

        if(name == ""){
            $("#edit-error_name").show();
            i++;
        }
        else if (!name.match(/^[A-Za-z0-9\s]+$/)) {
            $("#edit-error_name2").show();
            i++;
        }

        if(fullname == ""){
            $("#edit-error_fullname").show();
            i++;
        }
        else if (!fullname.match(/^[A-Za-z0-9\s]+$/)) {
            $("#edit-error_fullname2").show();
            i++;
        }

        if(email == ""){
            $("#edit-error_email").show();
            i++;
        }
        else if (!email.match(/^[\w -._]+@[\-0-9a-zA-Z_.]+?\.[a-zA-Z]{2,3}$/)) {
            $("#edit-error_email3").show();
            i++;
        }

        if(roleid == 0){
            $("#edit-error_roleid").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val()+"admin/update_user_details/",
                traditional: true,
                type: "post",
                dataType: "text",
                // data: {email: email, id:id, name:name, role:role},
                data: {sendData : JSON.stringify({username:username,fullname:fullname, roleid:roleid, email:email})},
                success: function (hasil) {
                    // var result = $.parseJSON(result);

                    var result = JSON.parse(hasil);
                    if(result.status=="success"){
                        location.reload();
                    }
                    else if(result.status=="exist"){
                        $("#edit-error_username2").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
        }
    }

    $( "#newProyekSubmit" ).click(function() {
        hideErrorMessages();
        show_loading();
        var i=0;
        var id_proyek = $('#id_proyek').val();
        var nama_proyek = $('#nama_proyek').val();
        var customer = $('#customer').val();
        var report_user = $('#report_user').val();
        var flagStatus = $('#flagStatus').val();
        console.log('MASUK');
        if(id_proyek == ""){
            $("#error_idproyek").show();
            i++;
        }

        if(nama_proyek == ""){
            $("#error_namaproyek").show();
            i++;
        }
        if(customer == ""){
            $("#error_customer").show();
            i++;
        }
        if(report_user == ""){
            $("#error_reportUser").show();
            i++;
        }
        if(flagStatus == 0){
            $("#error_flagStatus").show();
            i++;
        }

        if(i == 0){
            // var serial = serialize 
            $.ajax({
                url: $("#base-url").val() + "proyek/add_proyek",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {sendData : JSON.stringify({id_proyek:id_proyek,nama_proyek:nama_proyek, customer:customer, report_user:report_user, flagStatus:flagStatus})},
                success: function (hasil) { 
                    
                    var result = JSON.parse(hasil);
                    if(result.status=="success"){
                        location.reload();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                  
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
            
    });


