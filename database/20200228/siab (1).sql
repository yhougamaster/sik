-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2020 at 06:33 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siab`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblcustomer`
--

CREATE TABLE `tblcustomer` (
  `idCustomer` int(11) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `nopol` varchar(25) DEFAULT NULL,
  `typeMotor` varchar(1000) DEFAULT NULL,
  `usedKm` varchar(255) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcustomer`
--

INSERT INTO `tblcustomer` (`idCustomer`, `name`, `nopol`, `typeMotor`, `usedKm`, `idUser`) VALUES
(1, 'yhouga', 'N2407BM', 'Revo 110', '8000', NULL),
(2, 'asd', 'asd', 'asd', '123', NULL),
(3, 'qwe', 'qwe', 'qwe', '12341', NULL),
(4, 'aaa', 'aaa', '123', '123', NULL),
(5, 'yhouga123', 'asdasd', 'qsdqdad', '123124a', NULL),
(6, 'asd', '123123', 'asd', '123', NULL),
(7, 'asdasda', 'asdasd123', '12312asd', '12312312', NULL),
(8, 'a', 'a', 'a', '1', NULL),
(11, 'yhouga beta evantio', 'P21351BK', 'Vario125update', '24000', 8),
(12, 'ZXC', 'ZXC', 'ZXC', 'ZXC', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblemployee`
--

CREATE TABLE `tblemployee` (
  `idEmployee` int(11) NOT NULL,
  `nama` varchar(1000) DEFAULT NULL,
  `alamat` varchar(1000) DEFAULT NULL,
  `jabatan` varchar(1000) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `idUser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblemployee`
--

INSERT INTO `tblemployee` (`idEmployee`, `nama`, `alamat`, `jabatan`, `status`, `idUser`) VALUES
(1, 'Yhouga update', 'Jl. alamat update', 'Montir', 1, 0),
(2, 'Mekanik 2', 'Jl. mekanik', 'Montir', 1, 0),
(3, 'user1', 'user1', 'Admin', 1, 2),
(4, 'asd', 'asd', 'Montir', 1, 0),
(5, 'user2', 'user2', 'Admin', 1, 3),
(6, 'user3', 'user3', 'Admin', 1, 4),
(7, 'user4', 'user4', 'Admin', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tblqueue`
--

CREATE TABLE `tblqueue` (
  `idQueue` int(11) NOT NULL,
  `numQueue` int(11) DEFAULT NULL,
  `complaint` varchar(1000) DEFAULT NULL,
  `processingDate` datetime DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `idEmployee` int(11) DEFAULT NULL,
  `idCustomer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblqueue`
--

INSERT INTO `tblqueue` (`idQueue`, `numQueue`, `complaint`, `processingDate`, `lastUpdate`, `status`, `idEmployee`, `idCustomer`) VALUES
(1, 1, 'asd', '2019-12-25 00:00:00', NULL, 2, 2, 1),
(2, 2, 'asd', '2019-12-25 16:42:01', NULL, 2, 1, 2),
(3, 3, 'qwe', '2019-11-20 16:44:10', NULL, 2, 2, 3),
(4, 4, '123', '2019-12-25 16:44:31', NULL, 3, 2, 4),
(5, 5, '12312', '2019-12-25 16:52:16', NULL, 2, 2, 5),
(6, 1, 'asd', '2019-12-24 16:55:18', NULL, 0, 1, 6),
(7, 2, 'asd', '2019-12-24 16:55:34', NULL, 0, 2, 7),
(8, 3, 'a', '2019-12-24 16:55:59', NULL, 3, 4, 8),
(9, 6, 'asd', '2019-12-25 07:58:56', NULL, 3, 1, 11),
(10, 7, 'aaa', '2019-12-25 08:16:43', NULL, 3, 2, 11),
(11, 8, 'xxx', '2019-12-25 08:21:56', NULL, 3, 2, 11),
(12, 9, 'asd', '2019-12-25 08:23:35', NULL, 3, 1, 11),
(13, 10, 'zzz', '2019-12-25 08:23:51', NULL, 3, 4, 11),
(14, 11, 'bbb', '2019-12-25 08:24:17', NULL, 3, 4, 11),
(15, 12, 'zzz', '2019-12-25 08:24:41', NULL, 3, 1, 11),
(16, 13, 'asdasda', '2019-12-25 08:26:16', NULL, 3, 1, 11),
(17, 14, 'asd', '2019-12-25 08:27:20', NULL, 3, 1, 11),
(18, 15, 'asd', '2020-02-19 14:57:48', NULL, 2, 2, 11),
(19, 1, 'asd', '2020-01-06 17:17:28', NULL, 2, 1, 2),
(20, 2, 'ZXC', '2020-01-06 17:24:45', NULL, 3, 1, 12),
(21, 3, 'ASD', '2020-01-06 17:25:32', NULL, 0, 1, 2),
(22, 1, 'asd', '2020-02-28 18:03:40', NULL, 2, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tblservice`
--

CREATE TABLE `tblservice` (
  `idService` int(11) NOT NULL,
  `serviceType` int(11) DEFAULT NULL,
  `estimatedTime` varchar(20) DEFAULT NULL,
  `estimatedPrice` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `idQueue` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblservice`
--

INSERT INTO `tblservice` (`idService`, `serviceType`, `estimatedTime`, `estimatedPrice`, `price`, `startTime`, `endTime`, `idQueue`) VALUES
(1, 1, '30', '80000', NULL, '2019-12-25 16:00:59', NULL, 1),
(2, 2, '120', '120000', NULL, '2019-12-25 16:00:59', NULL, 1),
(3, 2, '120', '120000', NULL, '2019-12-25 16:42:01', NULL, 2),
(4, 2, '120', '120000', NULL, '2019-12-20 16:44:10', NULL, 3),
(5, 3, '120', '85000', NULL, '2019-12-25 16:44:31', NULL, 4),
(6, 2, '120', '120000', NULL, '2019-12-25 16:52:16', NULL, 5),
(7, 2, '120', '120000', NULL, '2019-12-24 16:55:18', NULL, 6),
(8, 1, '30', '80000', NULL, '2019-12-24 16:55:34', NULL, 7),
(9, 2, '120', '120000', NULL, '2019-12-24 16:55:59', NULL, 8),
(10, 1, '30', '80000', NULL, '2019-12-25 07:58:56', NULL, 9),
(11, 3, '120', '85000', NULL, '2019-12-25 08:16:43', NULL, 10),
(12, 2, '120', '120000', NULL, '2019-12-25 08:21:56', NULL, 11),
(13, 2, '120', '120000', NULL, '2019-12-25 08:23:35', NULL, 12),
(14, 2, '120', '120000', NULL, '2019-12-25 08:23:51', NULL, 13),
(15, 3, '120', '85000', NULL, '2019-12-25 08:24:17', NULL, 14),
(16, 1, '30', '80000', NULL, '2019-12-25 08:24:41', NULL, 15),
(17, 3, '120', '85000', NULL, '2019-12-25 08:26:16', NULL, 16),
(18, 1, '30', '80000', NULL, '2019-12-25 08:27:20', NULL, 17),
(19, 2, '120', '120000', NULL, '2020-02-19 14:57:48', NULL, 18),
(20, 1, '30', '80000', NULL, '2020-01-06 17:17:28', NULL, 19),
(21, 2, '120', '120000', NULL, '2020-01-06 17:17:28', NULL, 19),
(22, 3, '120', '85000', NULL, '2020-01-06 17:17:28', NULL, 19),
(23, 1, '30', '80000', NULL, '2020-01-06 17:24:45', NULL, 20),
(24, 2, '120', '120000', NULL, '2020-01-06 17:24:45', NULL, 20),
(25, 1, '30', '80000', NULL, '2020-01-06 17:25:32', NULL, 21),
(26, 2, '120', '120000', NULL, '2020-02-28 18:03:40', NULL, 22);

-- --------------------------------------------------------

--
-- Table structure for table `tblservicetype`
--

CREATE TABLE `tblservicetype` (
  `id` int(11) NOT NULL,
  `serviceName` varchar(255) NOT NULL,
  `serviceDescription` varchar(255) DEFAULT NULL,
  `servicePrice` varchar(255) NOT NULL,
  `serviceEstimatedTime` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblservicetype`
--

INSERT INTO `tblservicetype` (`id`, `serviceName`, `serviceDescription`, `servicePrice`, `serviceEstimatedTime`, `status`) VALUES
(1, 'Ganti Oli', 'Ganti oli dengan oli standard', '80000', '30', 1),
(2, 'Service ringan', 'cek dan ganti busi', '120000', '120', 1),
(3, 'Service Ringan', 'cek berkala dan pembersihan mesin', '85000', '120', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `idUser` int(11) NOT NULL,
  `username` varchar(1000) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `isLogin` tinyint(1) DEFAULT '0',
  `lastLogin` datetime DEFAULT NULL,
  `role` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`idUser`, `username`, `password`, `email`, `isLogin`, `lastLogin`, `role`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, 0, NULL, 99),
(2, 'user1', '24c9e15e52afc47c225b757e7bee1f9d', NULL, 0, NULL, 1),
(3, 'user2', '7e58d63b60197ceb55a1c487989a3720', NULL, 0, NULL, 1),
(4, 'user3', '92877af70a45fd6a2ed7fe81e1236b78', NULL, 0, NULL, 1),
(5, 'user4', '3f02ebe3d7929b091e3d8ccfde2f3bc6', NULL, 0, NULL, 1),
(8, 'yhougamaster', '4dae29ddbe57d9fa5e30cac42818d000', 'yhouga.13@gmail.com', 0, NULL, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  ADD PRIMARY KEY (`idCustomer`);

--
-- Indexes for table `tblemployee`
--
ALTER TABLE `tblemployee`
  ADD PRIMARY KEY (`idEmployee`);

--
-- Indexes for table `tblqueue`
--
ALTER TABLE `tblqueue`
  ADD PRIMARY KEY (`idQueue`);

--
-- Indexes for table `tblservice`
--
ALTER TABLE `tblservice`
  ADD PRIMARY KEY (`idService`),
  ADD KEY `idQueue` (`idQueue`);

--
-- Indexes for table `tblservicetype`
--
ALTER TABLE `tblservicetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  MODIFY `idCustomer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tblemployee`
--
ALTER TABLE `tblemployee`
  MODIFY `idEmployee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tblqueue`
--
ALTER TABLE `tblqueue`
  MODIFY `idQueue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tblservice`
--
ALTER TABLE `tblservice`
  MODIFY `idService` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tblservicetype`
--
ALTER TABLE `tblservicetype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tblservice`
--
ALTER TABLE `tblservice`
  ADD CONSTRAINT `tblservice_ibfk_1` FOREIGN KEY (`idQueue`) REFERENCES `tblqueue` (`idQueue`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
