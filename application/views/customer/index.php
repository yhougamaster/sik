<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/_all-skins.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>assets/web-v2/index.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url()?>assets/web-v2/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('username'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU UTAMA</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard Customer</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-pencil"></i>
            <span>Ubah Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("C_customer/edit_detail")?>"><i class="glyphicon glyphicon-list-alt"></i> Data Detail</a></li>
            <li><a href="<?=base_url("C_customer/edit_password")?>"><i class="glyphicon glyphicon-cog"></i> Password</a></li>
          </ul>
        </li>
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <input type="hidden" id="base-url" value="<?=base_url()?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Antrian</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="sisa-antrian">15</h3>

              <p>Sisa Antrian</p>
            </div>
            <div class="icon">
              <i class="icon ion-android-done"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="lama-tunggu"></h3>

              <p>Lama Tunggu</p>
            </div>
            <div class="icon">
              <i class="icon ion-gear-b"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="box">
            <div class="box-header">
              <button type="button" id="antrian-new" class="btn btn-primary" data-toggle="modal" data-target="#modal-addAntrian">Daftar </button>

              <!--------------------------
                | Alert |
              -------------------------->
              <div class="row">
                <div class="col-md-12">
                  <div class="alert alert-success" id="alert-success" style="display: none;">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong id="success-response"></strong>
                  </div>
                  <div class="alert alert-warning" id="alert-warning" style="display: none;">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong id="warning-response"></strong>
                  </div>
                </div>
              </div>

            </div>
            <div class="box-body">
              <table id="data-antrian" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id Antrian</th>
                  <th>Nama Pelanggan</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.row (main row) -->

      <!-- MODAL -->
      <div class="modal fade" id="modal-addAntrian">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Form Tambah Antrian</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="addPegawai">
                  <div class="box-body">
                    <div class="form-group col-md-12">
                      <label for="label-keluhan">Keluhan</label>
                      <textarea id="input-keluhan" class="form-control" rows="3" placeholder="Keluhan"></textarea>
                    </div>
                    <div class="form-group col-md-12">
                      <label>Layanan<span class="text-red" style="visibility: hidden" id="error-layanan"> *Layanan kosong</span></label>
                      <select class="form-control select2" id="input-layanan" multiple="multiple" data-placeholder="Pilih layanan"
                              style="width: 100%;">                                   
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="label-estimasiwaktu">Estimasi Waktu</label>
                      <input readonly="true" type="text" class="form-control" value="0" id="input-estimasiwaktu">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="label-estimasibiaya">Estimasi Biaya</label>
                      <input readonly="true" type="text" class="form-control" value="0" id="input-estimasibiaya">
                    </div>
                    <div class="form-group col-md-12">
                      <label for="label-mekanik">Mekanik<span class="text-red" style="visibility: hidden" id="error-mekanik"> *Mekanik belum dipilih</span></label>
                      <select id="input-mekanik" class="form-control" style="width: 100%;">
                      </select>
                    </div>
                  </div>
                </form>
            </div>
            <div class="modal-footer">
              <div class="form-group col-md-4 ">
                <button id="submit-antrian-new" type="button" class="btn btn-success pull-left ajax"><i id="loading-submit-new" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Submit</button>
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Batal</button>
              </div>       
            </div>
          </div>
          <!-- /.modal-content -->
        </div>    
      </div>
      <!-- END MODAL -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.18
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/dist/js/demo.js"></script>
<script>
  // variable
  var table = $('#data-antrian').DataTable({
       ajax:  {
          url: $("#base-url").val() + "C_antrian/get_list/customer",
          dataSrc: 'listAntrian'
       }, 
       columns: [
        { data: "numQueue" },
        { data: "name" },
        { data: "status" },
        { data: "action" },
       ] 
  })

  $(function () {
    var eventSelect = $('#input-layanan');
    $('#antrian-new').click(function () {
      empty_modal();
      $.ajax({
        url: $("#base-url").val() + "C_layanan/get_list/button",
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil);
          eventSelect.select2({
            data: result.listLayanan
          });
        }
      });
      $.ajax({
        url: $("#base-url").val() + "C_pegawai/get_list/button",
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil);
          $('#input-mekanik').find('option').remove();
          $('#input-mekanik').append("<option value='' disabled>Please select Mekanik</option>");
          $.each( result.listPegawai, function( key, val ) {
            $('#input-mekanik').append("<option value='"+val.idEmployee+"'>"+val.nama+"</option>");
          })
        }
      })
    
  
    });

    eventSelect.on("select2:unselect", function(e){
      var data = e.params.data;
      var splitStr = data.text.split("|");
      var getEstimasiWaktu = parseInt($('#input-estimasiwaktu').val());
      var getEstimasiBiaya = parseInt($('#input-estimasibiaya').val());
      getEstimasiWaktu = getEstimasiWaktu - parseInt(splitStr[2]);
      getEstimasiBiaya = getEstimasiBiaya - parseInt(splitStr[1]);
      $('#input-estimasiwaktu').val(getEstimasiWaktu);
      $('#input-estimasibiaya').val(getEstimasiBiaya);
    });

    eventSelect.on("select2:select", function (e) { 
        var data = e.params.data;
        var getEstimasiWaktu = parseInt($('#input-estimasiwaktu').val());
        var getEstimasiBiaya = parseInt($('#input-estimasibiaya').val());
        $.ajax({
          url: $("#base-url").val() + "C_layanan/get",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                id:data.id
                              })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            getEstimasiWaktu = getEstimasiWaktu + parseInt(result.serviceEstimatedTime);
            getEstimasiBiaya = getEstimasiBiaya + parseInt(result.servicePrice);
            $('#input-estimasiwaktu').val(getEstimasiWaktu);
            $('#input-estimasibiaya').val(getEstimasiBiaya);
          }
        });
    });

    $('#submit-antrian-new').click(function () {
      var cek = false;
      cek = validationInput();
      if (cek) {
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading-submit-new").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "C_antrian/ajax_add_customer",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                keluhan:$('#input-keluhan').val(),
                                layanan:$('#input-layanan').val(),
                                estimasiwaktu:$('#input-estimasiwaktu').val(),
                                estimasibiaya:$('#input-estimasibiaya').val(),
                                mekanik:$('#input-mekanik').val()
                            })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
              document.getElementById("loading-submit-new").style.display = "none";
              empty_modal();
              document.getElementById("alert-success").style.display = "block";
              $('#success-response').text(result.message);
              $('#modal-addAntrian').modal("toggle");
              table.ajax.reload();
              get_count_queue();
              change_state_button_daftar();
            } else {
              alert("Oops there is something wrong!");
            }
          }
        })
      }
    });
  });

  $( document ).ready(function(){
      get_count_queue();
      change_state_button_daftar();
  });

  function validationInput(){
      var cek = false;
      if($('#input-layanan').val() == null){
        document.getElementById("error-layanan").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-layanan").style.visibility = "hidden";
      }
      if($('#input-mekanik').val() == null){
        document.getElementById("error-mekanik").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-mekanik").style.visibility = "hidden";
      }
      
      return cek;
    }
    // additional function
    function empty_modal(){
        $('#input-keluhan').val("");
        $('#input-layanan').val("");
        $('#input-estimasiwaktu').val(0);
        $('#input-estimasibiaya').val(0);
        $('#input-mekanik').val(0);
    }

    function changeStatus(action, id){
      $.ajax({
          url: $("#base-url").val() + "C_antrian/ajax_change_status_queue",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                action:action,
                                id:id
                            })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
              document.getElementById("alert-success").style.display = "block";
              $('#success-response').text(result.message);
              table.ajax.reload();
              change_state_button_daftar();
              get_count_queue();
            } else {
              alert("Oops there is something wrong!");
            }
          }
        })
    }

    function change_state_button_daftar (){
      $.ajax({
          url: $("#base-url").val() + "C_antrian/check_cust_antrian",
          type: 'POST',
          success: function(data){
            var result = JSON.parse(data);
            if (result.check) {
              $('#antrian-new').attr("disabled", "disabled");
            } else {
              $('#antrian-new').removeAttr("disabled");
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
              var errorMsg = 'Ajax request failed: ' + xhr.responseText;
              document.getElementById("alert-warning").style.display = "block";
              $('#warning-response').html(errorMsg);
            }
      });
    }

    function get_count_queue() {
      $.ajax({
          url: $("#base-url").val() + "C_antrian/get_count_queue/customer",
          type: 'POST',
          success: function(data){
            var result = JSON.parse(data);
            document.getElementById("sisa-antrian").innerHTML = result.sisaAntrian;
            document.getElementById("lama-tunggu").innerHTML = result.lamaTunggu;
          },
          error: function (xhr, ajaxOptions, thrownError) {
              var errorMsg = 'Ajax request failed: ' + xhr.responseText;
              document.getElementById("alert-warning").style.display = "block";
              $('#warning-response').html(errorMsg);
            }
      });
    }

</script>
</body>
</html>
