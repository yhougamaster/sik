<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/_all-skins.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>assets/web-v2/index.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url()?>assets/web-v2/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('username'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU UTAMA</li>
        <li>
          <a href="<?=base_url("C_customer")?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard Customer</span>
          </a>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="glyphicon glyphicon-pencil"></i>
            <span>Ubah Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("C_customer/edit_detail")?>"><i class="glyphicon glyphicon-list-alt"></i> Data Detail</a></li>
            <li class="active"><a href="<?=base_url("C_customer/edit_password")?>"><i class="glyphicon glyphicon-cog"></i> Password</a></li>
          </ul>
        </li>
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <input type="hidden" id="base-url" value="<?=base_url()?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><small></small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Alert |
      -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success" id="alert-success" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="success-response"></strong>
          </div>
          <div class="alert alert-danger" id="alert-danger" style="display: none;">
            <a href="#" class="close" data-dismiss="alert"></a>
            <div id="danger-response"></div>
          </div>
        </div>
      </div>
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="col-md-5">
                <h2>
                    Ubah Password
                  </h2>
                <div class="box-body">
                  <form role="form" id="addPegawai">
                    <input type="hidden" class="form-control" id="input-id-pegawai" value="<?=$idCustomer?>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="label-nama">Old Password<span class="text-red" style="visibility: hidden" id="error-old-password"> *Old Password kosong</span></label>
                        <input type="password" class="form-control" id="input-old-password" placeholder="Old Password">
                      </div>
                      <div class="form-group">
                        <label for="label-alamat">New Password<span class="text-red" style="visibility: hidden" id="error-new-password"> *New Password kosong</span></label>
                        <input type="password" class="form-control" id="input-new-password" placeholder="New Password">
                      </div>
                      <div class="form-group">
                        <label for="label-tmpt-lahir">Retype New Password<span class="text-red" style="visibility: hidden" id="error-re-new-password"> *Retype New kosong</span></label>
                        <input type="password" class="form-control" id="input-re-new-password" placeholder="Retype New Password">
                      </div>
                    </div>
                  </form>
                </div>
                <div class="form-group col-md-4 ">
                    <button type="button" class="btn btn-success pull-left ajax" id="save-password"><i id="loading" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Save</button>
                    
                  </div>
              </div>
            </div>  
          </div>
        </div>  
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- PACE -->
<script src="<?=base_url()?>assets/web-v2/bower_components/PACE/pace.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#save-password').click(function () {
      document.getElementById("loading").style.display = "block";
      document.getElementById("error-old-password").style.visibility = "hidden";
      document.getElementById("error-new-password").style.visibility = "hidden";
      document.getElementById("error-re-new-password").style.visibility = "hidden";
      var cek = false;
      cek = validationInput();
      if(cek){
        alert('field ada yang kosong');
      } else {
        if( $('#input-new-password').val() == $('#input-re-new-password').val()){
          $.ajax({
              url: $("#base-url").val() + "C_customer/check_old_password",
              traditional: true,
              type: "post",
              dataType: "text", 
              data: {sendData : JSON.stringify({
                                    id_pegawai:$('#input-id-pegawai').val(),
                                    oldPassword:$('#input-old-password').val()
                                  })
                    },
              success: function (hasil) {
                var result = JSON.parse(hasil);
                if (result.status) {
                  $.ajax({
                    url: $("#base-url").val() + "C_customer/ajax_update_pegawai_password",
                    traditional: true,
                    type: "post",
                    dataType: "text", 
                    data: {sendData : JSON.stringify({
                                          id_pegawai:$('#input-id-pegawai').val(),
                                          newPassword:$('#input-new-password').val()
                                        })
                          },
                    success: function (hasil) {
                      var result = JSON.parse(hasil);
                      if(result.status=="success"){
                          document.getElementById("loading").style.display = "none";
                          document.getElementById("alert-success").style.display = "block";
                          $('#success-response').text(result.message);
                      }
                      else{
                          alert("Oops there is something wrong!");
                      }
                    }
                  })
                } else {
                  document.getElementById("loading").style.display = "none";
                  alert('Wrong old password');
                }
              }
            })
        } else {
          document.getElementById("error-re-new-password").style.visibility = "visible";
          alert('Re-type password is failed due to incorrect retype password');
        }
      }
    })

  });

  function validationInput() {
      var cek = false;
      if($('#input-old-password').val().length == 0){
        document.getElementById("error-old-password").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-old-password").style.visibility = "hidden";
      }
      if($('#input-new-password').val().length == 0){
        document.getElementById("error-new-password").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-new-password").style.visibility = "hidden";
      }
      if($('#input-re-new-password').val().length == 0){
        document.getElementById("error-re-new-password").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-re-new-password").style.visibility = "hidden";
      }
      if (cek) {
        document.getElementById("loading").style.display = "none";
      }
      return cek;
  }
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html> 