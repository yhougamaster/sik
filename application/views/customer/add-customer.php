<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Application Anabatic</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Pace style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/plugins/pace/pace.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/skin-blue.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>" class="logo">
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
      </div>

    </nav>
  
  </header>

  <input type="hidden"  id="base-url" value="<?=base_url()?>"/>
  <input type="hidden"  id="empty-id-pegawai" value="<?php echo $retVal = (empty($id_pegawai)) ? true : false; ?>"/>
  <?php 
    if ($this->session->userdata('logged_in')) {
      $sendedData = json_decode($data_pelanggan)[0];
  ?>
  <input type="hidden"  id="idPegawai" value="<?=$id_pegawai?>"/>
  <input type="hidden"  id="idUser" value="<?=$sendedData->idUser?>"/>
  <input type="hidden"  id="idCustomer" value="<?=$sendedData->idCustomer?>"/>
  <input type="hidden"  id="list-data-pegawai" value="<?php echo $data_pelanggan; ?>"/>
  <?php 
    }
  ?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url()?>assets/web-v2/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('username'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU UTAMA</li>
        <?php 
          if ($this->session->userdata('logged_in')) {
        ?>  
        <li>
          <a href="<?=base_url("C_customer")?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard Customer</span>
          </a>
        </li>
        <li id="menu-cuti" style="display: none"><a href="<?=base_url("dashboard/cuti")?>"><i class="glyphicon glyphicon-plane"></i> <span>Cuti</span></a></li>
        <li id="menu-timesheet-approval" style="display: none"><a href="<?=base_url("dashboard/timesheet_approval")?>"><i class="glyphicon glyphicon-list-alt"></i> <span>Approval Timesheet</span></a></li>
        <li id="menu-cuti-approval" style="display: none"><a href="<?=base_url("dashboard/cuti_approval")?>"><i class="glyphicon glyphicon-plane"></i> <span>Approval Cuti</span></a></li>
        <li class="active treeview menu-open">
          <a href="#">
            <i class="glyphicon glyphicon-pencil"></i>
            <span>Ubah Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="#"><i class="glyphicon glyphicon-list-alt"></i> Data Detail</a></li>
            <li><a href="<?=base_url("C_customer/edit_password")?>"><i class="glyphicon glyphicon-cog"></i> Password</a></li>
          </ul>
        </li>
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
        <?php 
          }
        ?>
       
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!--------------------------
    | yhouga content |
  -------------------------->

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><small></small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="col-md-10">
                <h2>
                    Data Pelanggan
                </h2>
                <div class="box-body">
                  <!--------------------------
                    | Alert |
                  -------------------------->
                  <div class="row">
                    <div class="col-md-12">
                      <div class="alert alert-success" id="alert-success" style="display: none;">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong id="success-response"></strong>
                      </div>
                      <div class="alert alert-warning" id="alert-warning" style="display: none;">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong id="warning-response"></strong>
                      </div>
                    </div>
                  </div>
                  <!-- end alert -->
                  <form role="form" id="addCustomer">
                    <div class="box-body">
                      <div class="form-group col-md-6">
                        <label for="label-nama">Nama Lengkap<span class="text-red" style="visibility: hidden" id="error-nama"> *Nama Lengkap kosong</span></label>
                        <input type="text" class="form-control" id="input-nama" placeholder="Nama" value="<?php if($this->session->userdata('logged_in')) { echo $sendedData->name; } ?>">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="label-username">Username<span class="text-red" style="visibility: hidden" id="error-username"> *Username kosong</span></label>
                        <input type="text" class="form-control" id="input-username" placeholder="Username" value="<?php if($this->session->userdata('logged_in')) { echo $sendedData->username; } ?>">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="label-merk">Merk Motor<span class="text-red" style="visibility: hidden" id="error-merk"> *Merk Motor kosong</span></label>
                        <input type="text" class="form-control" id="input-merk" placeholder="Merk Motor" value="<?php if($this->session->userdata('logged_in')) { echo $sendedData->typeMotor; } ?>">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="label-nopol">Nomor Polisi<span class="text-red" style="visibility: hidden" id="error-nopol"> *Nomor Polisi kosong</span></label>
                        <input type="text" class="form-control" id="input-nopol" placeholder="Nomor Polisi" value="<?php if($this->session->userdata('logged_in')) { echo $sendedData->nopol; } ?>">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="label-email">Email<span class="text-red" style="visibility: hidden" id="error-email"> *Email kosong</span></label>
                        <input type="email" class="form-control" id="input-email" placeholder="Email" value="<?php if($this->session->userdata('logged_in')) { echo $sendedData->email; } ?>">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="label-no-telp">KM Terpakai</label>
                        <input type="text" class="form-control" id="input-usedKm" placeholder="KM Terpakai" value="<?php if($this->session->userdata('logged_in')) { echo $sendedData->usedKm; } ?>">
                      </div>
                      <div class="form-group col-md-12" id="element-new-password" style="display: block">
                        <label for="label-alamat">New Password<span class="text-red" style="visibility: hidden" id="error-new-password"> *New Password kosong</span></label>
                        <input type="password" class="form-control" id="input-new-password" placeholder="New Password">
                      </div>
                      <div class="form-group col-md-12" id="element-new-repassword" style="display: block">
                        <label for="label-tmpt-lahir">Retype New Password<span class="text-red" style="visibility: hidden" id="error-re-new-password"> *Retype New kosong</span></label>
                        <input type="password" class="form-control" id="input-re-new-password" placeholder="Retype New Password">
                      </div>
                    </div>
                  </form>
                </div>
                <div class="form-group col-md-4 ">
                    <button type="button" class="btn btn-warning pull-left" id="submit-customer-new" style="display:block;"><i id="loading-new" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Daftar</button>
                    <button type="button" class="btn btn-warning pull-left" id="submit-customer-update" style="display:block;"><i id="loading-new" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Submit</button>
                    
                </div>
              </div>
              
            </div>  
          </div>
        </div>  
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- PACE -->
<script src="<?=base_url()?>assets/web-v2/bower_components/PACE/pace.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    if ($('#empty-id-pegawai').val() != 1) {
      document.getElementById("element-new-password").style.display = "none";
      document.getElementById("element-new-repassword").style.display = "none";
      document.getElementById("submit-customer-update").style.display = "block";
      document.getElementById("submit-customer-new").style.display = "none";
    }
    else {
      document.getElementById("element-new-password").style.display = "block";
      document.getElementById("element-new-repassword").style.display = "block";
      document.getElementById("submit-customer-update").style.display = "none";
      document.getElementById("submit-customer-new").style.display = "block";
    }

    $('#submit-customer-new').click(function () {
      var cek = false;
      cek = validationInput();
      if(cek){
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading-new").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "C_customer/ajax_add_customer",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                nama:$('#input-nama').val(),
                                username:$('#input-username').val(),
                                merk:$('#input-merk').val(),
                                nopol:$('#input-nopol').val(),
                                email:$('#input-email').val(),
                                usedKm:$('#input-usedKm').val(),
                                newPassword:$('#input-new-password').val(),
                                reNewPassword:$('#input-re-new-password').val(),                                
                              })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
              alert(result.message+" and will be redirected to login page in "+$('#base-url').val()+"customer");
              window.location = $('#base-url').val()+"customer";
            }
            else{
                alert("Oops there is something wrong!");
            }
          }
        })
      }
    })

  });

  $('#submit-customer-update').click(function () {
      var cek = false;
      cek = validationInput_update();
      if(cek){
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading-new").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "C_customer/ajax_update_customer",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                idUser:$('#idUser').val(),
                                idCustomer:$('#idCustomer').val(),
                                nama:$('#input-nama').val(),
                                username:$('#input-username').val(),
                                merk:$('#input-merk').val(),
                                nopol:$('#input-nopol').val(),
                                email:$('#input-email').val(),
                                usedKm:$('#input-usedKm').val()
                              })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
              var result = JSON.parse(hasil);
              document.getElementById("alert-success").style.display = "block";
              $('#success-response').text(result.message);
            }
            else{
                alert("Oops there is something wrong!");
            }
          }
        })
      }

  });
  
  function validationInput_update() { 
      var cek = false;
      if($('#input-nama').val().length == 0){
        document.getElementById("error-nama").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-nama").style.visibility = "hidden";
      }
      if($('#input-username').val().length == 0){
        document.getElementById("error-username").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-username").style.visibility = "hidden";
      }
      if($('#input-merk').val().length == 0){
        document.getElementById("error-merk").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-merk").style.visibility = "hidden";
      }
      if($('#input-nopol').val().length == 0){
        document.getElementById("error-nopol").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-nopol").style.visibility = "hidden";
      }
      if($('#input-email').val().length == 0){
        document.getElementById("error-email").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-email").style.visibility = "hidden";
      }
      return cek;
  }

  function validationInput() { 
      var cek = false;
      if($('#input-nama').val().length == 0){
        document.getElementById("error-nama").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-nama").style.visibility = "hidden";
      }
      if($('#input-username').val().length == 0){
        document.getElementById("error-username").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-username").style.visibility = "hidden";
      }
      if($('#input-merk').val().length == 0){
        document.getElementById("error-merk").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-merk").style.visibility = "hidden";
      }
      if($('#input-nopol').val().length == 0){
        document.getElementById("error-nopol").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-nopol").style.visibility = "hidden";
      }
      if($('#input-email').val().length == 0){
        document.getElementById("error-email").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-email").style.visibility = "hidden";
      }
      if($('#input-new-password').val().length == 0){
        document.getElementById("error-new-password").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-new-password").style.visibility = "hidden";
      }
      if($('#input-re-new-password').val().length == 0){
        document.getElementById("error-re-new-password").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-re-new-password").style.visibility = "hidden";
      }
      return cek;
  }

</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>