<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/_all-skins.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>assets/web-v2/index.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url()?>assets/web-v2/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('username'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU UTAMA</li>
        <li>
          <a href="<?=base_url("C_admin/index")?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard Antrian</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("C_admin/laporan/bulanan")?>"><i class="fa fa-circle-o"></i> Laporan Bulanan</a></li>
            <li><a href="<?=base_url("C_admin/laporan/mingguan")?>"><i class="fa fa-circle-o"></i> Laporan Mingguan</a></li>
            <li><a href="<?=base_url("C_admin/laporan/harian")?>"><i class="fa fa-circle-o"></i> Laporan Harian</a></li>
          </ul>
        </li>
        <?php 
          if ($this->session->userdata('role') == 99) {
        ?>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-list-ol"></i> <span>Kelola Layanan</span>
          </a>
        </li>
        <li>
          <a href="<?=base_url("C_admin/index_pegawai")?>">
            <i class="fa fa-list-ol"></i> <span>Kelola Pegawai</span>
          </a>
        </li>
        <?php
          }
        ?>
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman
        <small>Kelola Layanan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <input type="hidden"  id="base-url" value="<?=base_url()?>"/>
      <!-- Main row -->
      <div class="box">
            <div class="box-header">
              <button id="layanan-new" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-new">Tambah </button>

              <!--------------------------
                | Alert |
              -------------------------->
              <div class="row">
                <div class="col-md-12">
                  <div class="alert alert-success" id="alert-success" style="display: none;">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong id="success-response"></strong>
                  </div>
                  <div class="alert alert-warning" id="alert-warning" style="display: none;">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong id="warning-response"></strong>
                  </div>
                </div>
              </div>

            </div>
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table-layanan" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id Layanan</th>
                  <th>Nama Layanan</th>
                  <th>Harga</th>
                  <th>Estimasi Pengerjaan</th>
                  <th>Aksi</th>
                </tr>
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.row (main row) -->

      <!-- MODAL -->
      <div class="modal fade" id="modal-new">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form role="form">
                  <div class="box-body">
                    <input type="hidden" class="form-control" id="id" value="">
                    <div class="form-group col-md-12">
                      <label for="label-nama">Nama Layanan<span class="text-red" style="visibility: hidden" id="error-nama"> *Nama Lengkap kosong</span></label>
                      <input type="text" class="form-control" id="input-nama" placeholder="Nama">
                    </div>
                    <div class="form-group col-md-12">
                      <label for="label-deskirpsi">Deskripsi</label>
                      <textarea id="input-deskripsi" class="form-control" rows="3" placeholder="Deskripsi"></textarea>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="label-harga">Harga<span class="text-red" style="visibility: hidden" id="error-harga"> *Harga kosong</span></label>
                      <input type="text" class="form-control" id="input-harga" placeholder="Harga">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="label-waktu">Estimasi Waktu (menit)<span class="text-red" style="visibility: hidden" id="error-waktu"> *Waktu kosong</span></label>
                      <input type="number" class="form-control" id="input-waktu" placeholder="Estimasi Waktu">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <div class="form-group col-md-6 ">
                <button id="submit-layanan-new" type="button" class="btn btn-success pull-left ajax" style="display:none;"><i id="loading-submit-new" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Submit</button>
                <button id="submit-layanan-update" type="button" class="btn btn-primary pull-left ajax" style="display:none;"><i id="loading-submit-update" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Update</button>
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Batal</button>
              </div>       
            </div>
          </div>
          <!-- /.modal-content -->
        </div>    
      </div>
      <!-- END MODAL -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.18
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/web-v2/dist/js/demo.js"></script>
<script>
  // variable
  var table = $('#table-layanan').DataTable({
       ajax:  {
          url: $("#base-url").val() + "C_layanan/get_list/menu",
          dataSrc: 'listLayanan'
       }, 
       columns: [
        { data: "id" },
        { data: "serviceName" },
        { data: "servicePrice" },
        { data: "serviceEstimatedTime" },
        { data: "action" },
       ] 
  })

  // page function
  $(function () {
    $('#layanan-new').click(function () {
      $('.modal-title').text("Form Tambah Layanan");
      empty_modal();
      document.getElementById("submit-layanan-update").style.display = "none";
      document.getElementById("submit-layanan-new").style.display = "block";
    });

    $('#submit-layanan-new').click(function () {
      var cek = false;
      cek = validationInput();
      if (cek) {
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading-submit-new").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "C_layanan/ajax_add",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                nama_layanan:$('#input-nama').val(),
                                deskripsi:$('#input-deskripsi').val(),
                                harga:$('#input-harga').val(),
                                estimasi_waktu:$('#input-waktu').val()
                            })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
              document.getElementById("loading-submit-new").style.display = "none";
              empty_modal();
              document.getElementById("alert-success").style.display = "block";
              $('#success-response').text(result.message);
              $('#modal-new').modal("toggle");
              table.ajax.reload();
            } else {
              alert("Oops there is something wrong!");
            }
          }
        })
      }
    });

    $('#submit-layanan-update').click(function () {
      var cek = false;
      cek = validationInput();
      if (cek) {
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading-submit-update").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "C_layanan/ajax_update/" + $("#id").val(),
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                nama_layanan:$('#input-nama').val(),
                                deskripsi:$('#input-deskripsi').val(),
                                harga:$('#input-harga').val(),
                                estimasi_waktu:$('#input-waktu').val()
                            })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
              document.getElementById("loading-submit-update").style.display = "none";
              empty_modal();
              document.getElementById("alert-success").style.display = "block";
              $('#success-response').text(result.message);
              $('#modal-new').modal("toggle");
              table.ajax.reload();
            } else {
              alert("Oops there is something wrong!");
            }
          }
        })
      }
    });

  })

  // additional function
  function empty_modal(){
      $('#id').val("");
      $('#input-nama').val("");
      $('#input-deskripsi').val("");
      $('#input-harga').val("");
      $('#input-waktu').val("");
  }

  function fill_modal(arrayData){
      $('#id').val(arrayData['id']);
      $('#input-nama').val(arrayData['serviceName']);
      $('#input-deskripsi').val(arrayData['serviceDescription']);
      $('#input-harga').val(arrayData['servicePrice']);
      $('#input-waktu').val(arrayData['serviceEstimatedTime']);
  }

  function getDataEdit(id){
    $.ajax({
      url: $("#base-url").val() + "C_layanan/get",
      traditional: true,
      type: "post",
      dataType: "text", 
      data: {sendData : JSON.stringify({
                            id:id
                          })
            },
      success: function (hasil) {
        var result = JSON.parse(hasil);
        fill_modal(result);
        $('.modal-title').text("Form Ubah Layanan");
        document.getElementById("submit-layanan-update").style.display = "block";
        document.getElementById("submit-layanan-new").style.display = "none";
        $('#modal-new').modal("toggle");
      }
    })
  }

  function dataDeletion(id){
    var submit = confirm("Are you sure to delete this data?");
    if (submit) {
      $.ajax({
        url: $("#base-url").val() + "C_layanan/ajax_delete/" + id,
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil);
          document.getElementById("alert-success").style.display = "block";
          $('#success-response').text(result.message);
          table.ajax.reload();
        }
      })
    }
  } 

  function validationInput(){
    var cek = false;
    if($('#input-nama').val().length == 0){
      document.getElementById("error-nama").style.visibility = "visible";
      cek = true;
    } else {
      document.getElementById("error-nama").style.visibility = "hidden";
    }
    if($('#input-harga').val().length == 0){
      document.getElementById("error-harga").style.visibility = "visible";
      cek = true;
    } else {
      document.getElementById("error-harga").style.visibility = "hidden";
    }
    if($('#input-waktu').val().length == 0){
      document.getElementById("error-waktu").style.visibility = "visible";
      cek = true;
    } else {
      document.getElementById("error-waktu").style.visibility = "hidden";
    }
    return cek;
  }

</script>
</body>
</html>
