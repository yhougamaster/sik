<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_laporan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_list($report_type, $date){

        switch ($report_type) {
            case 'Bulanan':
                return $this->db->query("select month, count(*) as countTrx, sum(sum) as total 
                                            from 
                                                (
                                                    SELECT count(idQueue), sum(estimatedPrice) as sum, month(startTime) as month 
                                                    FROM tblservice 
                                                    where idQueue in (SELECT idQueue FROM tblqueue where status = 2 and year(processingDate) = ".$date.") GROUP BY idQueue
                                                ) a
                                            GROUP BY month")->result_array();
                break;
            case 'Mingguan':
                return $this->db->query("select weekNumber as week, count(a.idQueue) as countTrx, sum(a.sumPrice) as total from (select week(processingDate) as weekNumber, s.idQueue, sum(s.estimatedPrice) as sumPrice from tblqueue q join tblservice s on q.idQueue = s.idQueue  where week(processingDate) between ".$date." group by s.idQueue) a group by weekNumber")->result_array();
                break;
            case 'Harian':
                return $this->db->query("select date, count(*) as countTrx, sum(sum) as total 
                                            from 
                                                (
                                                    SELECT count(idQueue), sum(estimatedPrice) as sum, date(startTime) as date 
                                                    FROM tblservice 
                                                    where idQueue in (SELECT idQueue FROM tblqueue where status = 2 and date(processingDate) BETWEEN '".$date[0]."' and '".$date[1]."') GROUP BY idQueue
                                                ) a
                                            GROUP BY date")->result_array();
                break;
        }
    }

    

}

/* End of file */
