<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_pegawai extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_list_all($mekanik){
        if ($mekanik) {
            $this->db->select('*');
            $this->db->from('tblemployee');
            $this->db->where('status', "1");
            $this->db->where('jabatan', "Montir");

            return $this->db->get()->result_array();
        } else {

            $this->db->select('*');
            $this->db->from('tblemployee');

            return $this->db->get()->result_array();
        }
    }

    function get($id){
        $this->db->select('*');
        $this->db->from('tblemployee');
        $this->db->where('status', "1");
        $this->db->where('idEmployee', $id);

        return $this->db->get()->result_array()[0];
    }

    // crud
    function transac($type, $postData){
        if ($type == 3) {
            $data_employee = array( 'status' => 0, );
        } else {
            $data_employee = array(
                'nama' => $postData->nama,
                'alamat' => $postData->alamat,
                'jabatan' => $postData->jabatan,
                'status' => 1,
                'idUser' => 0
            );
        }

        if ($type == 1) {
            if ($postData->jabatan == 'Admin') {
                $data_user = array(
                    'username' => $postData->username,
                    'password' => md5($postData->password),
                    'email' => null,
                    'isLogin' => 0,
                    'lastLogin' => null,
                    'role' => 1,
                );
                $this->db->insert('tbluser', $data_user);
                $data_employee['idUser'] = $this->db->insert_id();
            }
            $this->db->insert('tblemployee', $data_employee);

            $message = 'New data successfully inserted';
        }
        elseif ($type == 2) {
            $this->db->where('idEmployee', $postData->id);
            $this->db->update('tblemployee', $data_employee);

            $message = 'Data successfully updated';
        }
        elseif ($type == 3) {
            $this->db->where('idEmployee', $postData);
            $this->db->update('tblemployee', $data_employee);

            $message = 'Data successfully deleted';
        }
        
        return array('status' => 'success', 'message' => $message);

    }


    // additional function
    function generate_password(){
        $chars = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ023456789!@#$%^&*()_=";
        $password = substr( str_shuffle( $chars ), 0, 10 );

        return $password;
    }
}

/* End of file */
