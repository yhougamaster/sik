<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_layanan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_list_all(){
        $this->db->select('*');
        $this->db->from('tblservicetype');
        $this->db->where('status', "1");

        return $this->db->get()->result_array();
    }

    function get($id){
        $this->db->select('*');
        $this->db->from('tblservicetype');
        $this->db->where('status', "1");
        $this->db->where('id', $id);

        return $this->db->get()->result_array();
    }

    // crud
    function transac($type, $postData){
        if ($type == 3) {
            $data = array( 'status' => 0, );
        } else {
            $data = array(
                'serviceName' => $postData->nama_layanan,
                'servicePrice' => $postData->harga,
                'serviceDescription' => $postData->deskripsi,
                'serviceEstimatedTime' => $postData->estimasi_waktu,
                'status' => 1,
            );
        }

        if ($type == 1) {
            $this->db->insert('tblservicetype', $data);
            $message = 'New data successfully inserted';
        }
        elseif ($type == 2) {
            $this->db->where('id', $postData->id);
            $this->db->update('tblservicetype', $data);
            $message = 'Data successfully updated';
        }
        elseif ($type == 3) {
            $this->db->where('id', $postData);
            $this->db->update('tblservicetype', $data);
            $message = 'Data successfully deleted';
        }
        
        return array('status' => 'success', 'message' => $message);

    }

}

/* End of file */
