<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_user extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get($id){
        $this->db->select('*');
        $this->db->from('tbluser');
        $this->db->where('idUser', $id);

        return $this->db->get()->result_array()[0];
    }

    public function update_password($postData)
    {
        $data_user = array(
                'password' => md5($postData->newPassword),
            );
        $this->db->where('idUser', $postData->id_pegawai);
        $this->db->update('tbluser', $data_user);

        return $data = array(
            'status' => 'success',
            'message' => 'Password berhasil diubah',
        );
    }
}

/* End of file */
