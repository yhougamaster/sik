<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_pelanggan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_list_all(){
        $this->db->select('*');
        $this->db->from('tblcustomer');

        return $this->db->get()->result_array();
    }

    function get($id){
        $this->db->select('*');
        $this->db->from('tblcustomer');
        $this->db->where('idUser', $id);

        return $this->db->get()->result_array();
    }
    function get_user($id){
        $this->db->select('*');
        $this->db->from('tbluser');
        $this->db->where('idUser', $id);

        return $this->db->get()->result_array();
    }

    function get_byNopol($nopol){
        $this->db->select('*');
        $this->db->from('tblcustomer');
        $this->db->where('nopol', $nopol);

        return $this->db->get()->result_array();
    }

    // crud
    function transac($type, $postData){
        if ($type == 1) {
            $data_user = array(
                'username' => $postData->username,
                'password' => md5($postData->newPassword),
                'email' => $postData->email,
                'isLogin' => 0,
                'lastLogin' => null,
                'role' => 2,
            );
            $this->db->insert('tbluser', $data_user);
            
            $data_customer = array(
                'name' => $postData->nama,
                'nopol' => $postData->nopol,
                'typeMotor' => $postData->merk,
                'usedKm' => $postData->usedKm,
            );
            $data_customer['idUser'] = $this->db->insert_id();
            $this->db->insert('tblcustomer', $data_customer);

            $message = "Daftar Berhasil";
        }

        return array('status' => 'success', 'message' => $message);

    }

    function update_customer($postData){
        $data_user = array(
            'username' => $postData->username,
            'email' => $postData->email,
        );
        $this->db->where('idUser', $postData->idUser);
        $this->db->update('tbluser', $data_user);
        
        $data_customer = array(
            'name' => $postData->nama,
            'nopol' => $postData->nopol,
            'typeMotor' => $postData->merk,
            'usedKm' => $postData->usedKm,
        );
        $this->db->where('idCustomer', $postData->idCustomer);
        $this->db->update('tblcustomer', $data_customer);

        $message = "Daftar Berhasil";

        return array('status' => 'success', 'message' => $message);

    }

}

/* End of file */
