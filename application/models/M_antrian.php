<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_antrian extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_list(){
        return $this->db->query('select 
    q.idQueue, c.idUser as idCust, q.numQueue, c.name, c.nopol, c.typeMotor, e.nama, q.status
from 
    tblqueue q join tblcustomer c on q.idCustomer = c.idCustomer
               join tblemployee e on q.idEmployee = e.idEmployee
where date(q.processingDate) = CURRENT_DATE')->result_array();
    }

    function get($id){
        $this->db->select('*');
        $this->db->from('tblservicetype');
        $this->db->where('status', "1");
        $this->db->where('id', $id);

        return $this->db->get()->result_array();
    }

    function getQueue($id){
        $this->db->select('*');
        $this->db->from('tblqueue');
        $this->db->where('idQueue', $id);
        return $this->db->get()->result_array();
    }

    function get_last_queue(){
        return $this->db->query('SELECT COALESCE(MAX(numQueue), 0)+1 as numQueue from tblqueue where date(processingDate) = CURRENT_DATE')->result_array()[0]['numQueue'];
    }

    // crud
    function insertAntrian($postData){
        $this->load->model('m_pelanggan', 'pelanggan');
        $getPelanggan = $this->pelanggan->get_byNopol($postData->plat);
        if ($getPelanggan == null) {
            $data_customer = array(
                'name' => $postData->nama,
                'nopol' => $postData->plat,
                'typeMotor' => $postData->merk,
                'usedKm' => $postData->km,
            );

            $this->db->insert('tblcustomer', $data_customer);
            $idCustomer = $this->db->insert_id();
        } else {
            $idCustomer = $getPelanggan[0]['idCustomer'];
        }
        $data_queue = array(
            'numQueue' => $this->get_last_queue(),
            'complaint' => $postData->keluhan,
            'processingDate' => date('Y-m-d H:i:s'),
            'status' => 0,
            'idCustomer' => $idCustomer,
            'idEmployee' => $postData->mekanik
        );

        $this->db->insert('tblqueue', $data_queue);
        $idQueue = $this->db->insert_id();
        
        $this->load->model('m_layanan', 'layanan');
        foreach ($postData->layanan as $key => $value) {
            $getLayanan = $this->layanan->get($value)[0];
            $data_service = array(
                'serviceType' => $value,
                'estimatedTime' => $getLayanan['serviceEstimatedTime'],
                'estimatedPrice' => $getLayanan['servicePrice'],
                'startTime' => date('Y-m-d H:i:s'),
                'idQueue' => $idQueue,
            );
            $this->db->insert('tblservice', $data_service);
        }

        return array('status' => 'success', 'message' => 'Antrian berhasil');

    }

    function insertAntrianCust($postData){
        $this->load->model('m_pelanggan', 'pelanggan');
        $getPelanggan = $this->pelanggan->get($this->session->userdata('id'));
        $idCustomer = $getPelanggan[0]['idCustomer'];
        $data_queue = array(
            'numQueue' => $this->get_last_queue(),
            'complaint' => $postData->keluhan,
            'processingDate' => date('Y-m-d H:i:s'),
            'status' => 0,
            'idCustomer' => $idCustomer,
            'idEmployee' => $postData->mekanik
        );

        $this->db->insert('tblqueue', $data_queue);
        $idQueue = $this->db->insert_id();
        
        $this->load->model('m_layanan', 'layanan');
        foreach ($postData->layanan as $key => $value) {
            $getLayanan = $this->layanan->get($value)[0];
            $data_service = array(
                'serviceType' => $value,
                'estimatedTime' => $getLayanan['serviceEstimatedTime'],
                'estimatedPrice' => $getLayanan['servicePrice'],
                'startTime' => date('Y-m-d H:i:s'),
                'idQueue' => $idQueue,
            );
            $this->db->insert('tblservice', $data_service);
        }

        return array('status' => 'success', 'message' => 'Antrian berhasil');

    }


    function updateAntrian ($postData){
        $data_queue = array(
                'status' => $postData->action,
            );
        $this->db->where('idQueue', $postData->id);
        $this->db->update('tblqueue', $data_queue);


        if ($postData->action == 2) {
            $message = "Sepeda anda telah selesai di service, silahkan melakukan pembayaran di kasir. Terimakasih.";
            $subject = "Pengambilan Servis";
            $list_data_queue = $this->getQueue($postData->id)[0];
            $idUser = $this->db->query("select idUser from tblcustomer where idCustomer = ".$list_data_queue['idCustomer'])->result_array()[0]["idUser"];
            $email = $this->db->query("select email from tbluser where idUser = ".$idUser)->result_array()[0]["email"];
            $this->send_email($message,$subject,$email);
        }
        return array('status' => 'success', 'message' => 'Update Antrian berhasil');
    }

    function get_count_queue(){
        return $this->db->query('SELECT (select count(status) FROM `tblqueue` where date(processingDate) = CURRENT_DATE and status = 2) as done,  (select count(status) FROM `tblqueue` where date(processingDate) = CURRENT_DATE and status = 1) as progress, (select count(status) FROM `tblqueue` where date(processingDate) = CURRENT_DATE and status = 0) as queue, (select count(status) FROM `tblqueue` where date(processingDate) = CURRENT_DATE and status = 3) as cancel')->result_array();
    }

    function get_count_queue_customer(){
        return $this->db->query('select 
    count(t.numQueue) as sisaAntrian, sum(totalWaktu) as lamaTunggu
from 
(select 
    q.numQueue, sum(s.estimatedTime) as totalWaktu
from 
    tblqueue q join tblservice s on q.idQueue = s.idQueue and date(q.processingDate) = CURRENT_DATE and (q.status not in (3,2))
group BY
    q.numQueue) t')->result_array();
    }

    function check_cust_antrian(){
        return $this->db->query('select 
    true 
from 
    tblqueue q join tblcustomer c on q.idCustomer = c.idCustomer and date(q.processingDate) = CURRENT_DATE and (q.status not in (3,2))
               join tbluser u on c.idUser = u.idUser')->result_array();
    }

    function send_email($message,$subject,$sendTo){
        require_once APPPATH.'libraries\mailer\class.phpmailer.php';
        require_once APPPATH.'libraries\mailer\class.smtp.php';
        require_once APPPATH.'libraries\mailer\mailer_config.php';
        include APPPATH.'libraries\mailer\template\template.php';
        
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        try
        {
            $mail->SMTPDebug = 1;  
            $mail->SMTPAuth = true; 
            $mail->SMTPSecure = 'ssl'; 
            $mail->Host = HOST;
            $mail->Port = PORT;  
            $mail->Username = GUSER;  
            $mail->Password = GPWD;     
            $mail->SetFrom(GUSER, 'Administrator');
            $mail->Subject = "DO NOT REPLY - ".$subject;
            $mail->IsHTML(true);   
            $mail->WordWrap = 0;


            $hello = '<h1 style="color:#333;font-family:Helvetica,Arial,sans-serif;font-weight:300;padding:0;margin:10px 0 25px;text-align:center;line-height:1;word-break:normal;font-size:38px;letter-spacing:-1px">Hello, &#9786;</h1>';
            $thanks = "<br><br><i>This is autogenerated email please do not reply.</i><br/><br/>Thanks,<br/>Admin<br/><br/>";

            $body = $hello.$message.$thanks;
            $mail->Body = $header.$body.$footer;
            $mail->AddAddress($sendTo);

            if(!$mail->Send()) {
                $error = 'Mail error: '.$mail->ErrorInfo;
                return array('status' => false, 'message' => $error);
            } else { 
                return array('status' => true, 'message' => '');
            }
        }
        catch (phpmailerException $e)
        {
            $error = 'Mail error: '.$e->errorMessage();
            return array('status' => false, 'message' => $error);
        }
        catch (Exception $e)
        {
            $error = 'Mail error: '.$e->getMessage();
            return array('status' => false, 'message' => $error);
        }
        
    }

}

/* End of file */
