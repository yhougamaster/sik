<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cuti extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }

        // if($this->session->userdata('roleid') != '1'){
        //     redirect(base_url());
        // }

        $this->load->model('cuti_model', 'cuti');
    }
    
    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    function submit_cuti()
    {
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $this->load->model('pegawai_model', 'pegawai');
        $postData->idPegawai = $this->session->userdata('id'); 
        $insert = $this->cuti->insert_cuti($postData);

        echo json_encode($insert);
    }

    function count_total_leave()
    {
        $this->load->model('pegawai_model', 'pegawai');
        $data_pegawai = $this->pegawai->get_pegawai_by_id_pegawai($this->session->userdata('id'))[0];
        $getCount = $this->cuti->count_total_leave($data_pegawai);
        $getDataUsedLeave = $this->cuti->count_used_leave($data_pegawai);
        $getUsedLeave = 0;
        foreach ($getDataUsedLeave as $data) {
            $getUsedLeave += $data['total'];
        }
        $return = array(
            'status' => 'success', 
            'count' => $getCount-$getUsedLeave, 
        );
        echo json_encode($return);
    }    

    function get_cuti_list()
    {
        $this->ajax_checking(); 
        $get_data = $this->cuti->get_cuti_list($this->session->userdata('id'));
        for ($i=0; $i < count($get_data); $i++) { 
            $get_data[$i]['button'] = '';
            if ($get_data[$i]['status'] == 0) {
                $get_data[$i]['status_conv'] = '<button type="button" class="btn btn-block btn-primary btn-sm" disabled>Submited</button>';
            } else if ($get_data[$i]['status'] == 2){
                $get_data[$i]['status_conv'] = '<button type="button" class="btn btn-block btn-success btn-sm" disabled>Approved</button>';
            } else if ($get_data[$i]['status'] == 3){
                $get_data[$i]['status_conv'] = '<button type="button" class="btn btn-block btn-danger btn-sm" disabled>Rejected</button>';
            }
            $get_data[$i]['back_to_work'] = $get_data[$i]['end_leave'];
            $get_data[$i]['end_leave'] = date('Y-m-d', strtotime($get_data[$i]['start_leave']. ' + '.$get_data[$i]['total'].' days'));
            if ($get_data[$i]['status'] == 0) {
                $get_data[$i]['button'] = 
                            '<table>
                              <tr>
                                <td><button type="button" class="btn btn-block btn-danger btn-sm" onclick="dataDeletion('.$get_data[$i]['id_cuti'].')"><i class="glyphicon glyphicon-trash"></i></button></td>
                              </tr>
                            </table>';
            }
        }
        $return = array(
                'list_cuti' => $get_data, 
            );

        echo json_encode($return);
    }

    function get_cuti_list_approval($id_pegawai, $id_proyek)
    {
        $this->ajax_checking(); 
        $get_data = $this->cuti->get_cuti_list_approval($id_pegawai, $id_proyek);
        for ($i=0; $i < count($get_data); $i++) {
            $get_data[$i]['back_to_work'] = $get_data[$i]['end_leave'];
            $get_data[$i]['end_leave'] = date('Y-m-d', strtotime($get_data[$i]['start_leave']. ' + '.$get_data[$i]['total'].' days'));
            if ($get_data[$i]['status'] == 0) {
                $get_data[$i]['button'] = 
                            '<table>
                              <tr>
                                <td><button type="button" class="btn btn-block btn-success btn-sm" onclick="cuti_approve('.$get_data[$i]['id_cuti'].')"> Approve</button></td>
                                <td><button type="button" class="btn btn-block btn-danger btn-sm" onclick="cuti_reject('.$get_data[$i]['id_cuti'].')"> Reject</button></td>
                              </tr>
                            </table>';
            } else if ($get_data[$i]['status'] == 2) {
                $get_data[$i]['button'] = 
                            '<table>
                              <tr>
                                <td><button type="button" class="btn btn-block btn-success btn-sm" disabled> Approved</button></td>
                              </tr>
                            </table>';
            } else if ($get_data[$i]['status'] == 3) {
                $get_data[$i]['button'] = 
                            '<table>
                              <tr>
                                <td><button type="button" class="btn btn-block btn-danger btn-sm" disabled> Reject</button></td>
                              </tr>
                            </table>';
            }
        }
        $return = array(
                'list_cuti' => $get_data, 
            );

        echo json_encode($return);
    }

    function cancel_leave()
    {
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $result = $this->cuti->delete_cuti($postData->idCuti);

        echo json_encode($result);
    }

    function cuti_approval($statusApproval)
    {
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $postData->status = $statusApproval;
        $result = $this->cuti->cuti_approval($postData);

        echo json_encode($result);

    }

}

/* End of file */
