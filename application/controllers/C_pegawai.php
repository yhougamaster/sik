<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_pegawai extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('m_pegawai', 'pegawai');
    }

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    // get
    public function get_list($action){
        $this->ajax_checking();
        $mekanik = false;
        if ($action == 'menu') {
            $get_data = $this->pegawai->get_list_all($mekanik);
            for ($i=0; $i < count($get_data); $i++) {
                switch ($get_data[$i]['status']) {
                    case 0:
                        $get_data[$i]['status'] = "Tidak Aktif";
                        break;
                    case 1:
                        $get_data[$i]['status'] = "Aktif";
                        break;
                }
                $get_data[$i]['action'] = '<table>
                              <tr>
                                <td><button type="button" class="btn btn-block btn-warning btn-sm" onclick="getDataEdit('.$get_data[$i]['idEmployee'].')"><i class="glyphicon glyphicon-pencil"></i></button></td>
                                <td><button type="button" class="btn btn-block btn-danger btn-sm" onclick="dataDeletion('.$get_data[$i]['idEmployee'].')"><i class="glyphicon glyphicon-trash"></i></button></td>
                              </tr>
                            </table>';
            }
        }
        if ($action == 'button') {
            $mekanik = true;
            $get_data = $this->pegawai->get_list_all($mekanik);
        }
        $data = array(
            'row' => count($get_data),
            'listPegawai' => $get_data
        );
        
        echo json_encode($data);
    }

    public function get(){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $get_data = $this->pegawai->get($postData->id);
        if ($get_data['jabatan'] == 'Admin') {
            $this->load->model('m_user', 'user');
            $get_data_user = $this->user->get($get_data['idUser']);
            $get_data['username'] = $get_data_user['username'];
            $get_data['password'] = $get_data_user['password'];
        }
        
        echo json_encode($get_data);
    }

    // add
    function ajax_add(){
        if($this->session->userdata('role') != '99'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $insert = $this->pegawai->transac(1, $postData);
        echo json_encode($insert);
    } 

    //update
    function ajax_update($id){
        if($this->session->userdata('role') != '99'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $postData->id = $id;
        $update = $this->pegawai->transac(2, $postData);
        echo json_encode($update);
    } 

    //delete
    function ajax_delete($id){
        if($this->session->userdata('role') != '99'){
            redirect(base_url());
        }
        $delete = $this->pegawai->transac(3, $id);
        echo json_encode($delete);
    }

}

/* End of file */
