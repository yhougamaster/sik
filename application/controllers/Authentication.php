<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Authentication extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model("authentication_model");
    }

    public function index() {
        if(!$this->session->userdata('logged_in')) {
            $data = array('alert' => false);
            $this->load->view('login',$data);
        } 
    }

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    public function login(){
        $message = "";
        $postData = $this->input->post();
        $validate = $this->authentication_model->validate_login($postData);
        if ($validate){
            // set session data
            $newdata = array(
                'username'     => $validate['username'],
                'role' => $validate['role'],
                'id' => $validate['idUser'],
                'logged_in' => TRUE,
              
            );
            $this->session->set_userdata($newdata);
            redirect(base_url("home"));
        } else {
            $message = "user not found or invalid username / password";
        }
        $data = array(
                'alert' => true,
                'message' => $message
            );
        $this->load->view('login',$data);
     
    }

    function customer(){
        if(!$this->session->userdata('logged_in')) {
            $data = array('alert' => false);
            $this->load->view('customer/login',$data);
        }    
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function register(){
        $this->load->view('customer/add-customer', $this->session->userdata());
    }


}

/* End of file */
