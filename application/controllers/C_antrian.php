<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_antrian extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('m_antrian', 'antrian');
    }

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    // get
    public function get_list($action){
        $this->ajax_checking();
        $get_data = $this->antrian->get_list($action);
        if ($action == 'admin') {
            for ($i=0; $i < count($get_data); $i++) {
                switch ($get_data[$i]['status']) {
                    case 1:
                        $get_data[$i]['status'] = 'Dalam Proses';
                        $get_data[$i]['action'] = '<table>
                              <tr>
                                <td>
                                    <button type="button" onclick="changeStatus(2, '.$get_data[$i]['idQueue'].')" class="btn btn-primary pull-left"></i>Ubah Selesai</button>
                                </td>
                              </tr>
                            </table>';
                        break;
                    case 2:
                        $get_data[$i]['status'] = 'Selesai';
                        $get_data[$i]['action'] = '<table>
                              <tr>
                                <td>
                                    <button type="button" disabled="true" class="btn btn-success pull-left">Selesai</button>
                                </td>
                              </tr>
                            </table>';
                        break;
                    case 3:
                        $get_data[$i]['status'] = 'Batal';
                        $get_data[$i]['action'] = '<table>
                              <tr>
                                <td>
                                    <button type="button" disabled="true" class="btn btn-danger pull-left">Batal</button>
                                </td>
                              </tr>
                            </table>';
                        break;
                    
                    default:
                        $get_data[$i]['status'] = 'Menunggu Antrian';
                        $get_data[$i]['action'] = '<table>
                              <tr>
                                <td>
                                    <button type="button" onclick="changeStatus(3, '.$get_data[$i]['idQueue'].')" class="btn btn-danger pull-left">Batal</button>
                                    <button type="button" onclick="changeStatus(1, '.$get_data[$i]['idQueue'].')" class="btn btn-primary pull-left"></i>Progress</button>
                                </td>
                              </tr>
                            </table>';
                        break;
                }
            }
        } else {
            for ($i=0; $i < count($get_data); $i++) {
                switch ($get_data[$i]['status']) {
                    case 1:
                        $get_data[$i]['status'] = 'Dalam Proses';
                        $get_data[$i]['action'] = '';
                        break;
                    case 2:
                        $get_data[$i]['status'] = 'Selesai';
                        $get_data[$i]['action'] = '<table>
                              <tr>
                                <td>
                                    <button type="button" disabled="true" class="btn btn-success pull-left">Selesai</button>
                                </td>
                              </tr>
                            </table>';
                        break;
                    case 3:
                        $get_data[$i]['status'] = 'Batal';
                        $get_data[$i]['action'] = '<table>
                              <tr>
                                <td>
                                    <button type="button" disabled="true" class="btn btn-danger pull-left">Batal</button>
                                </td>
                              </tr>
                            </table>';
                        break;
                    
                    default:
                        $get_data[$i]['status'] = 'Menunggu Antrian';
                        if ($get_data[$i]['idCust'] == $this->session->userdata('id')) {
                            $get_data[$i]['action'] = '<table>
                                  <tr>
                                    <td>
                                        <button type="button" onclick="changeStatus(3, '.$get_data[$i]['idQueue'].')" class="btn btn-danger pull-left">Batal</button>
                                    </td>
                                  </tr>
                                </table>';
                        } else {
                            $get_data[$i]['action'] = '';
                        }
                        break;
                }
            }
        }
        
        $data = array(
            'row' => count($get_data),
            'listAntrian' => $get_data
        );
        
        echo json_encode($data);
    }

    public function ajax_add(){
        if($this->session->userdata('role') != '99'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        
        $insert = $this->antrian->insertAntrian($postData);
        echo json_encode($insert);      
    }

    public function ajax_add_customer(){
        if($this->session->userdata('role') != '2'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        
        $insert = $this->antrian->insertAntrianCust($postData);
        echo json_encode($insert);      
    }

    public function ajax_change_status_queue(){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));

        $status_update = $this->antrian->updateAntrian($postData);
        echo json_encode($status_update);
    }

    public function get_count_queue($page){
        if ($page == 'admin') {
            echo json_encode($this->antrian->get_count_queue()[0]);
        } else {
            $data = $this->antrian->get_count_queue_customer()[0];
            $jam = round((($data['lamaTunggu'])/60),0,PHP_ROUND_HALF_DOWN);
            $menit = $data['lamaTunggu']%60;
            $data['lamaTunggu'] = $jam." jam ".$menit." menit";
            echo json_encode($data);
        }
    }

    public function check_cust_antrian(){
        $return = array('check' => false );
        $data = $this->antrian->check_cust_antrian();
        if ($data) {
            $return['check'] = true;
        }
        
        echo json_encode($return);
    }
    
}

/* End of file */
