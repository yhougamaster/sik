<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_customer extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('m_pelanggan', 'pelanggan');
    }

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    public function index(){
        $this->load->view('customer/index');
    }

    public function edit_detail()
    {
        // kurang get user
        $data_get = $this->pelanggan->get($this->session->userdata('id'));
        $data_user = $this->pelanggan->get_user($data_get[0]['idUser']);
        $data_get[0]['username'] = $data_user[0]['username'];
        $data_get[0]['email'] = $data_user[0]['email'];

        $data['data_pelanggan'] = json_encode($data_get);
        $data['id_pegawai'] = $this->session->userdata('id');
        $this->load->view('customer/add-customer', $data);
    }

    public function edit_password()
    {
        $data = array(
                    'idCustomer' => $this->session->userdata('id'), 
                );
        $this->load->view('customer/edit-password', $data);
    }

    public function check_old_password()
    {
        $data = array(
            'status' => true,
        );
        $this->load->model('m_user', 'user');
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $get_data_pelanggan = $this->user->get($this->session->userdata('id'));
        if ($get_data_pelanggan['password'] != md5($postData->oldPassword)) {
            $data['status'] = false;
        } 
        echo json_encode($data);
    }

    public function ajax_update_pegawai_password()
    {
        $this->load->model('m_user', 'user');
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $data = $this->user->update_password($postData);
        echo json_encode($data);
    }

    // add
    function ajax_add_customer(){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $insert = $this->pelanggan->transac(1, $postData);
        echo json_encode($insert);
    } 

    // update
    function ajax_update_customer(){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $update = $this->pelanggan->update_customer($postData);
        echo json_encode($update);
    } 

    

}

/* End of file */
