<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_admin extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
    }

    public function index(){
        $this->load->view('admin/index');
    }

    public function index_layanan(){
        $this->load->view('admin/layanan');
    }

    public function index_pegawai(){
        $this->load->view('admin/pegawai');
    }

    public function laporan($type){
        switch ($type) {
            case 'bulanan':
                $this->load->view('admin/laporan-bulanan');
                break;
            case 'mingguan':
                $this->load->view('admin/laporan-mingguan');
                break;
            case 'harian':
                $this->load->view('admin/laporan-harian');
                break;
        }
    }



}

/* End of file */
