<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pegawai extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }

        // if($this->session->userdata('roleid') != '1'){
        //     redirect(base_url());
        // }

        $this->load->model('pegawai_model');
        $this->load->helper('string');
    }
    

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    public function pegawai_list(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }

        $data = array(
            'formTitle' => 'Pegawai Management',
            'title' => 'Pegawai Management',
            'pegawais' => $this->pegawai_model->get_pegawai_list(),
        );

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');
        $this->load->view('pegawai/pegawai_list', $data);

    }

    // ajax
    function ajax_pegawai_list(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $get_data = $this->pegawai_model->ajax_get_pegawai_list();
        for ($i=0; $i < count($get_data); $i++) { 
            if ($get_data[$i]['roleid'] == 1) {
                $get_data[$i]['roleid'] = 'Admin';
            } else if ($get_data[$i]['roleid'] == 2) {
                $get_data[$i]['roleid'] = 'Pegawai';
            }
            $get_data[$i]['button'] = 
                        '<table>
                          <tr>
                            <td><button type="button" class="btn btn-block btn-warning btn-sm" onclick="getDataEdit(\''.$get_data[$i]['id_pegawai'].'\')"><i class="glyphicon glyphicon-pencil"></i></button></td>
                            <td><button type="button" class="btn btn-block btn-danger btn-sm" onclick="dataDeletion('.$get_data[$i]['id_pegawai'].',\''.$get_data[$i]['username'].'\',\''.$get_data[$i]['nik'].'\')"><i class="glyphicon glyphicon-trash"></i></button></td>
                            <td><button type="button" class="btn btn-block btn-default btn-sm" onclick="resetPassword(\''.$get_data[$i]['id_pegawai'].'\',\''.$get_data[$i]['nik'].'\')">Reset Password</button></td>
                          </tr>
                        </table>';
        }
        $data = array(
            'row' => count($get_data),
            'listPegawai' => $get_data
        );

        echo json_encode($data);
    }

    function ajax_list_approver(){
        $get_data = $this->pegawai_model->ajax_get_approver_list();

        echo json_encode($get_data);
    }

    function ajax_list_non_approver(){
        $get_data = $this->pegawai_model->ajax_list_non_approver();
        $front_end = array();
        $back_end = array();
        $tester = array();
        $business_analyst = array();
        
        foreach ($get_data as $key) {
            $data = array(
                'id' => $key['id_pegawai'],
                'text' => $key['nama_pegawai']
            );
            if ($key['jabatan'] == "Front End Programmer") {
                array_push($front_end, $data);
            } else if ($key['jabatan'] == "Back End Programmer"){
                array_push($back_end, $data);
            } else if ($key['jabatan'] == "Business Analyst"){
                array_push($business_analyst, $data);
            } else if ($key['jabatan'] == "Tester"){
                array_push($tester, $data);
            }
        }
        $return = array(
            [
                'text' => 'Front End Programmer',
                'children' => $front_end,
            ],
            [
                'text' => 'Back End Programmer',
                'children' => $back_end,
            ],
            [
                'text' => 'Business Analyst',
                'children' => $business_analyst,
            ],
            [
                'text' => 'Tester',
                'children' => $tester,
            ], 
        );
        echo json_encode($return);
    }

    function ajax_list_TeamByProyek(){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $get_data = $this->pegawai_model->get_pegawai_team_by_idProyek($postData->key);
        $string_fe = "<dt>Front End</dt>";
        $count_fe = 0;
        $string_be = "<dt>Back End</dt>";
        $count_be = 0;
        $string_ba = "<dt>Business Analyst</dt>";
        $count_ba = 0;
        $string_t = "<dt>Tester</dt>";
        $count_t = 0;
        foreach ($get_data as $key) {
            if ($key['jabatan'] == "Front End Programmer") {
                $string_fe .= "<dd>".$key['nama_pegawai']."</dd>";
                $count_fe++;
            } else if ($key['jabatan'] == "Back End Programmer"){
                $string_be .= "<dd>".$key['nama_pegawai']."</dd>";
                $count_be++;
            } else if ($key['jabatan'] == "Business Analyst"){
                $string_ba .= "<dd>".$key['nama_pegawai']."</dd>";
                $count_ba++;
            } else if ($key['jabatan'] == "Tester"){
                $string_t .= "<dd>".$key['nama_pegawai']."</dd>";
                $count_t++;
            }
        }
        if ($count_fe == 0) {
            $string_fe .= "<dd></dd>";
        } else if ($count_be == 0){
            $string_be .= "<dd></dd>";
        } else if ($count_ba == 0){
            $string_ba .= "<dd></dd>";
        } else if ($count_t == 0){
            $string_t .= "<dd></dd>";
        }
        // var_dump($string_fe.$string_be.$string_ba.$string_t);
        // exit();
        $arrayName = array('string' => $string_fe.$string_be.$string_ba.$string_t);
        echo json_encode($arrayName);
    }

    function approver_ajax_list_teamByProject($menu, $key_id_proyek, $id_proyek, $nama_proyek){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $get = $this->pegawai_model->get_pegawai_team_by_idProyek($key_id_proyek);
        $get_list = "";
        if (count($get) > 0) {
            for ($i=0; $i < count($get); $i++) { 
                // if ($menu == 'cuti') {
                //     $this->load->model('cuti_model', 'cuti');
                //     $get_submited_cuti = $this->cuti->get_submited_pegawai_proyek_cuti($get[$i]['id_pegawai'], $key_id_proyek);
                //     if (count($get_submited_cuti) > 0) {
                //         $get_list .= '<tr>
                //                         <td>'.$id_proyek.'</td>
                //                         <td>'.$nama_proyek.'</td>
                //                         <td>'.$get[$i]['nama_pegawai'].'</td>
                //                         <td><a href="'.base_url('dashboard/apporver_cuti_detail/'.$get[$i]['id_pegawai'].'/'.$key_id_proyek).'"><button type="button" class="btn btn-primary">Detail Cuti</button></a></td>
                //                      </tr>';
                //     }
                // } else if ($menu == 'timesheet') {
                //     $this->load->model('timesheet_model', 'timesheet');
                //     $get_submited_timesheet = $this->timesheet->get_submited_pegawai_proyek_timesheet($get[$i]['id_pegawai'], $key_id_proyek);
                //     if (count($get_submited_timesheet) > 0) {
                //         $get_list .= '<tr>
                //                         <td>'.$id_proyek.'</td>
                //                         <td>'.$nama_proyek.'</td>
                //                         <td>'.$get[$i]['nama_pegawai'].'</td>
                //                         <td><a href="'.base_url('dashboard/apporver_timesheet_week_detail/'.$get[$i]['id_pegawai'].'/'.$key_id_proyek).'"><button type="button" class="btn btn-primary">Detail Timesheet</button></a></td>
                //                      </tr>';
                //     }
                // }
                if ($menu == 'cuti') {
                    $get_list .= '<tr>
                                <td>'.$id_proyek.'</td>
                                <td>'.$nama_proyek.'</td>
                                <td>'.$get[$i]['nama_pegawai'].'</td>
                                <td><a href="'.base_url('dashboard/apporver_cuti_detail/'.$get[$i]['id_pegawai'].'/'.$key_id_proyek).'"><button type="button" class="btn btn-primary"> Cuti</button></a></td>
                             </tr>';
                } else if ($menu == 'timesheet') {
                    $get_list .= '<tr>
                                <td>'.$id_proyek.'</td>
                                <td>'.$nama_proyek.'</td>
                                <td>'.$get[$i]['nama_pegawai'].'</td>
                                <td><a href="'.base_url('dashboard/apporver_timesheet_week_detail/'.$get[$i]['id_pegawai'].'/'.$key_id_proyek).'"><button type="button" class="btn btn-success"> Timesheet</button></a></td>
                             </tr>';
                }
                               
            }
        }
        
        // if ($get_list == "") {
        //     if ($menu == 'cuti') {
        //         $get_list = "<tr><td colspan='4'>No cuti submited</td></tr>";
        //     } else if ($menu == 'timesheet') {
        //         $get_list = "<tr><td colspan='4'>No timesheet submited</td></tr>";
        //     } 
        // }
        if ($get_list == "") {
            $get_list = "<tr><td colspan='4'>No Pegawai Exist</td></tr>"; 
        }
        $get_data = array('list_team' => $get_list, );
        
        echo json_encode($get_data);
    }
    
    function add_pegawai(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $insert = $this->pegawai_model->insert_pegawai($postData);
      //  $insert_user = $this->pegawai_model->insert_userpg($postData);
        if($insert['status'] == "success")
            $this->session->set_flashdata('success', 'Pegawai '.$postData->nama_pegawai.' has been successfully created!');
        
        echo json_encode($insert);
    }

    function ajax_add_pegawai(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $postData->nik = "ATIC-".$this->pegawai_model->get_last_id()['id'];
        // $postData->username = random_string('alnum', 8);
        $insert = $this->pegawai_model->insert_pegawai($postData);
        
        echo json_encode($insert);
    }

    function ajax_update_pegawai(){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $insert = $this->pegawai_model->update_pegawai_details($postData);
        
        echo json_encode($insert);
    }

    function update_pegawai_details(){
        $this->ajax_checking();

        $postData = $this->input->post('sendData');
        $update = $this->pegawai_model->update_pegawai_details($postData);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'Pegawai '.$postData['nama_lengkap'].'`s details have been successfully updated!');

        echo json_encode($update);
    }

    function deactivate_pegawai($username,$id){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();

        $update = $this->pegawai_model->deactivate_pegawai($username,$id);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'User '.$username.' has been successfully deleted!');

        echo json_encode($update);
    }

    function ajax_deactivate_pegawai(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $update = $this->pegawai_model->ajax_deactivate_pegawai($postData);

        echo json_encode($update);
    }

    function reset_pegawai_password($email,$id){
        $this->ajax_checking();

        $update = $this->pegawai_model->reset_pegawai_password($email,$id);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'User '.$email.'`s password has been successfully reset!');

        echo json_encode($update);
    }

    function ajax_reset_pegawai_password(){
        $this->ajax_checking();

        $postData = json_decode($this->input->post('sendData'));
        $update = $this->pegawai_model->ajax_reset_pegawai_password($postData);

        echo json_encode($update);
    }

    function check_old_password(){
        $this->ajax_checking();

        $postData = json_decode($this->input->post('sendData'));
        $password = $this->pegawai_model->get_pegawai_by_id_pegawai($postData->id_pegawai)[0]['password'];
        $cek_str = true;
        if (md5($postData->oldPassword) != $password) {
            $cek_str = false;
        }

        $cek = array('status' => $cek_str, );
        echo json_encode($cek);
    }

    function ajax_update_pegawai_password(){
        $this->ajax_checking();

        $postData = json_decode($this->input->post('sendData'));
        $result = $this->pegawai_model->ajax_update_pegawai_password($postData);

        echo json_encode($result);
    }
    
    function ajax_get_pegawai_data(){
        $this->ajax_checking();

        $postData = json_decode($this->input->post('sendData'));
        $update = $this->pegawai_model->get_pegawai_by_id_pegawai($postData->idPegawai)[0];
        echo json_encode($update);
    }

    // function activity_log(){
    //     $data = array(
    //         'formTitle' => 'Activity Log',
    //         'title' => 'Activity Log',
    //     );
    //     $this->load->view('frame/header_view');
    //     $this->load->view('frame/sidebar_nav_view');
    //     $this->load->view('admin/activity_log', $data);

    // }

    // function get_activity_log(){
    //     $this->ajax_checking();
    //     echo  json_encode( $this->admin_model->get_activity_log() );
    // }
}

/* End of file */
