<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
    }

    public function index(){
        switch ($this->session->userdata('role')) {
            case 1:
                $this->pegawai();
                break;
            case 2:
                $this->customer();
                break;
            case 99:
                $this->admin();
                break;
        }
    }

    function admin(){
        redirect(base_url('C_admin'));
    }

    function customer(){
        redirect(base_url('C_customer'));
    }

    function pegawai(){
        redirect(base_url('C_pegawai'));
    }

}

/* End of file */
