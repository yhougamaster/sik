<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
    }

    public function admin() {
        $data = array(
                    'role_string' => $this->getRole(),
                );
        $this->load->view('v2/pegawai', $data);
    }

    public function projek() {
        $data = array(
                    'role_string' => $this->getRole(),
                );
        $this->load->view('v2/projek', $data);
    }

    public function pegawai() {
        $data = array(
                    'role_string' => $this->getRole(),
                );
        $this->load->view('v2/timesheet', $data);
    }

    public function edit_detailPegawai() {
        $data = array(
                'id_pegawai' => $this->session->userdata('id'),
                'role' => $this->session->userdata('roleid'),
                'role_string' => $this->getRole(),
            );
        $this->load->view('v2/edit-pegawai', $data);
    }

    public function edit_passwordPegawai() {
          $data = array(
                'id_pegawai' => $this->session->userdata('id'),
                'role' => $this->session->userdata('roleid'),
                'role_string' => $this->getRole(),
            );
        $this->load->view('v2/edit-password', $data);
    }

    public function cuti() {
        $data = array(
                    'role_string' => $this->getRole(),
                );
        $this->load->view('v2/cuti', $data);
    }

    public function timesheet_detail($id_week_timesheet, $start, $end)
    {
        $this->load->model('pegawai_model', 'pegawai');
        $get_pegawai = $this->pegawai->get_pegawai_by_id_pegawai($this->session->userdata('id'))[0];
        $show_button_pegawai = true;
        if ($get_pegawai['jabatan'] == 'Project Manager') {
            $show_button_pegawai = false;
        }
        
        $data = array(
                'show_button_pegawai' => $show_button_pegawai,
                'id_week_timesheet' => $id_week_timesheet,
                'start_date' => $start,
                'end_date' => $end, 
                'role_string' => $this->getRole(),
            );
        $this->load->view('v2/timesheet-detail', $data);
    }

    public function timesheet_approval() {
        $data = array(
                'role_string' => $this->getRole(),
            );
        $this->load->view('v2/timesheet-approval', $data);
    }

    public function cuti_approval() {
        $data = array(
                'role_string' => $this->getRole(),
            );
        $this->load->view('v2/cuti-approval', $data);
    }

    public function apporver_timesheet_week_detail($id_pegawai, $id_proyek)
    {
        $data = array(
            'id_pegawai' => $id_pegawai,
            'id_proyek' => $id_proyek,
            'role_string' => $this->getRole(),  
        );
        $this->load->view('v2/timesheet-week-approval', $data);
    }

    public function apporver_cuti_detail($id_pegawai, $id_proyek)
    {
        $data = array(
            'id_pegawai' => $id_pegawai,
            'id_proyek' => $id_proyek,  
            'role_string' => $this->getRole(),
        );
        $this->load->view('v2/cuti_detail-approval', $data);
    }

    public function apporver_timesheet_week_day_detail($id_pegawai, $id_proyek, $id_week_timesheet, $start, $end)
    {
        $data = array(
            'id_pegawai' => $id_pegawai,
            'id_proyek' => $id_proyek,
            'id_week_timesheet' => $id_week_timesheet,
            'start_date' => $start,
            'end_date' => $end,
            'role_string' => $this->getRole(),
        );
        $this->load->view('v2/timesheet-detail-approval', $data);
    }

    // updated to v2
    // public function index() {
    //     $this->load->view('frame/header_view');
    //     $this->load->view('frame/sidebar_nav_view');
    //     $this->load->view('dashboard');
    //     $this->load->view('frame/footer_view');
    // }

    public function index()
    {
        $isAdmin = true;
        $isProjectManager = false;
        $isPegawai = false;

        $this->load->model('pegawai_model', 'pegawai');
        $get_data_pegawai = $this->pegawai->get_pegawai_by_id_pegawai($this->session->userdata('id'));
        if ($get_data_pegawai != null) {
            $get_pegawai = $get_data_pegawai[0];
            if ($get_pegawai['jabatan'] == 'Project Manager') {
                $isProjectManager = true;    
            } else if ($get_pegawai['jabatan'] != 'Project Manager') {
                $isPegawai = true;
            }
            $isAdmin = false;
        } 

        $data = array(
                'isAdmin' => $isAdmin,
                'isProjectManager' => $isProjectManager,
                'isPegawai' => $isPegawai,
                'role_string' => $this->getRole(),
            );
        $this->load->view('v2/dashboard', $data);
    }

    public function getRole()
    {
        $this->load->model('pegawai_model', 'pegawai');
        $get_pegawai = $this->pegawai->get_pegawai_by_id_pegawai($this->session->userdata('id'));
        if (empty($get_pegawai)) {
            return 'Administrator';
        } else {
            $get_pegawai = $this->pegawai->get_pegawai_by_id_pegawai($this->session->userdata('id'))[0];
            if ($get_pegawai['roleid'] == '1') {
                return 'Administrator';
            } else if ($get_pegawai['roleid'] == '2'){
                return 'Pegawai';
            }
        }
    }


}

/* End of file */
