<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_layanan extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('m_layanan', 'layanan');
    }

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    // get
    public function get_list($action){
        $this->ajax_checking();
        $get_data = $this->layanan->get_list_all();
        if ($action == 'menu') {
            for ($i=0; $i < count($get_data); $i++) {
                $get_data[$i]['action'] = '<table>
                              <tr>
                                <td><button type="button" class="btn btn-block btn-warning btn-sm" onclick="getDataEdit('.$get_data[$i]['id'].')"><i class="glyphicon glyphicon-pencil"></i></button></td>
                                <td><button type="button" class="btn btn-block btn-danger btn-sm" onclick="dataDeletion('.$get_data[$i]['id'].')"><i class="glyphicon glyphicon-trash"></i></button></td>
                              </tr>
                            </table>';
            }
        }
        if ($action == 'button') {
            $temp_result = array();
            for ($i=0; $i < count($get_data); $i++) {
                $temp = array(
                            'id'    => $get_data[$i]['id'],
                            'text'  => $get_data[$i]['serviceName'] . " | " . $get_data[$i]['servicePrice'] . " | " . $get_data[$i]['serviceEstimatedTime'] . " | Menit" 
                        );
                array_push($temp_result, $temp);
            }
            $get_data = $temp_result;
        }
        $data = array(
            'row' => count($get_data),
            'listLayanan' => $get_data
        );
        
        echo json_encode($data);
    }

    public function get(){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $get_data = $this->layanan->get($postData->id);
        $data = array(
            'row' => count($get_data),
            'listLayanan' => $get_data[0]
        );
        
        echo json_encode($get_data[0]);
    }

    // add
    function ajax_add(){
        if($this->session->userdata('role') != '99'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $insert = $this->layanan->transac(1, $postData);
        echo json_encode($insert);
    } 

    //update
    function ajax_update($id){
        if($this->session->userdata('role') != '99'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $postData->id = $id;
        $update = $this->layanan->transac(2, $postData);
        echo json_encode($update);
    } 

    //delete
    function ajax_delete($id){
        if($this->session->userdata('role') != '99'){
            redirect(base_url());
        }
        $delete = $this->layanan->transac(3, $id);
        echo json_encode($delete);
    }

}

/* End of file */
