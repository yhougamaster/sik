<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class C_laporan extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model('m_laporan', 'laporan');
    }

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    // get
    public function get_list($report_type, $search){
        $get_data = array();
        if ($report_type == 'Bulanan') {
            $dt = date('Y');
            $get_data = $this->laporan->get_list($report_type, $dt);
            for ($i=0; $i < count($get_data); $i++) {
                $monthNum  = $get_data[$i]['month'];
                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                $get_data[$i]['month'] = $dateObj->format('F'); // March
                $get_data[$i]['action'] = '<table>
                                  <tr>
                                    <td><button type="button" class="btn btn-block btn-warning btn-sm" onclick="getDetail()">Detail</button></td>
                                  </tr>
                                </table>';
            }
        } else if ($report_type == 'Mingguan') {
            $dateSearch = date('d-'.$search.'-Y');
            $arr = $this->get_firstday_lastday($dateSearch);
            $dt = $arr['firstWeek']." and ".$arr['lastWeek'];
            $firstWeek = $arr['firstWeek'];
            $lastWeek = $arr['lastWeek'];
            $get_data = $this->laporan->get_list($report_type, $dt);
            for ($i=0; $i < count($get_data); $i++) {
                $counter = 1;
                // tambahan untuk bisa show tiap minggu ketika itu kosong
                for ($j=$firstWeek; $j <= $lastWeek; $j++) { 
                    if ($get_data[$i]['week'] == $j) {
                        $get_data[$i]['week'] = "Minggu ke ".$counter;
                    }
                    $counter++;
                }
                $get_data[$i]['action'] = '<table>
                                  <tr>
                                    <td><button type="button" class="btn btn-block btn-warning btn-sm" onclick="getDetail()">Detail</button></td>
                                  </tr>
                                </table>';
            }
        } else if ($report_type == 'Harian') {
            $duedt = explode("to",$search);
            $get_data = $this->laporan->get_list($report_type, $duedt);
            for ($i=0; $i < count($get_data); $i++) {
                $get_data[$i]['action'] = '<table>
                                  <tr>
                                    <td><button type="button" class="btn btn-block btn-warning btn-sm" onclick="getDetail()">Detail</button></td>
                                  </tr>
                                </table>';
            }
        }
      
        $data = array(
            'row' => count($get_data),
            'listData' => $get_data
        );
        
        echo json_encode($data);
    }

    public function ajax_add(){
        if($this->session->userdata('role') != '99'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        
        $insert = $this->antrian->insertAntrian($postData);
        echo json_encode($insert);      
    }

    public function ajax_add_customer(){
        if($this->session->userdata('role') != '2'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        
        $insert = $this->antrian->insertAntrianCust($postData);
        echo json_encode($insert);      
    }

    public function ajax_change_status_queue(){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));

        $status_update = $this->antrian->updateAntrian($postData);
        echo json_encode($status_update);
    }

    public function get_count_queue($page){
        if ($page == 'admin') {
            echo json_encode($this->antrian->get_count_queue()[0]);
        } else {
            $data = $this->antrian->get_count_queue_customer()[0];
            $jam = round((($data['lamaTunggu'])/60),0,PHP_ROUND_HALF_UP);
            $menit = $data['lamaTunggu']%60;
            $data['lamaTunggu'] = $jam." jam ".$menit." menit";
            echo json_encode($data);
        }
    }

    public function check_cust_antrian(){
        $return = array('check' => false );
        $data = $this->antrian->check_cust_antrian();
        if ($data) {
            $return['check'] = true;
        }
        
        echo json_encode($return);
    }

    function get_firstday_lastday($date){

        // First day of the month.
        $firstDateMonth = date('Y-m-01', strtotime($date));
        $arr_return['firstWeek'] = date('W', strtotime($firstDateMonth));

        // Last day of the month.
        $lastDateMonth = date('Y-m-t', strtotime($date));
        $arr_return['lastWeek'] = date('W', strtotime($lastDateMonth));

        return $arr_return;
    }
    
}

/* End of file */
